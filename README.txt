# planty-awwwesomes
> Project made on basis of layout proposed by Awwwesomes on https://the-awwwesomes.gitbooks.io/html-css-step-by-step/content/pl/appendix/layouts/index.html .
I read the layout's properties using https://www.photopea.com/ tool and fixed properties for mobile, as they were not given.

## Table of contents
* [General info](#general-info)
* [Features](#features)
* [Status](#status)
* [Inspiration](#inspiration)
* [Contact](#contact)

## General info
It is my second awwwesomes' project coded to exercise and first one, where I used Javascript.

## Features
List of features ready and TODOs for future development
* Styles are done for >1440px

To-do list:
* Form validation
* Responsiveness

## Status
Project is: _in progress_

## Inspiration
Project based on https://the-awwwesomes.gitbooks.io/html-css-step-by-step/content/pl/appendix/layouts/index.html

## Contact
Created by mrsKandyzowana - feel free to contact me!
